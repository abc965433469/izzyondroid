package in.sunilpaulmathew.izzyondroid.utils;

import android.content.Context;
import android.content.pm.PackageManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import in.sunilpaulmathew.sCommon.CommonUtils.sCommonUtils;
import rikka.shizuku.Shizuku;
import rikka.shizuku.ShizukuRemoteProcess;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on October 31, 2022
 */
public class ShizukuInstaller {

    private static ShizukuRemoteProcess mProcess = null;

    public static boolean isShizukuInstallation() {
        return Shizuku.pingBinder() && Shizuku.checkSelfPermission() == PackageManager.PERMISSION_GRANTED;
    }

    private static String runCommand(String command) {
        StringBuilder output = new StringBuilder();
        try {
            mProcess = Shizuku.newProcess(new String[] {"sh", "-c", command}, null, null);
            BufferedReader mInput = new BufferedReader(new InputStreamReader(mProcess.getInputStream()));
            BufferedReader mError = new BufferedReader(new InputStreamReader(mProcess.getErrorStream()));
            String line;
            while ((line = mInput.readLine()) != null) {
                output.append(line).append("\n");
            }
            while ((line = mError.readLine()) != null) {
                output.append(line).append("\n");
            }
            mProcess.waitFor();
        } catch (Exception e) {
            output.append(e.getMessage()).append("\n");
        }
        return output.toString();
    }

    public static void destroyProcess() {
        if (mProcess != null) mProcess.destroy();
    }

    public static void installAPK(File apkFile, Context context) {
        runCommand("cp " + apkFile.getAbsolutePath() + " /data/local/tmp/APK.apk");
        sCommonUtils.saveString("installationStatus", runCommand("pm install /data/local/tmp/APK.apk"), context);
        runCommand("rm -r /data/local/tmp/APK.apk");
    }

}