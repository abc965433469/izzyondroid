package in.sunilpaulmathew.izzyondroid.utils.tasks;

import android.app.Activity;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.widget.LinearLayoutCompat;

import com.google.android.material.textview.MaterialTextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.sCommon.CommonUtils.sCommonUtils;
import in.sunilpaulmathew.sCommon.CommonUtils.sExecutor;
import in.sunilpaulmathew.sCommon.FileUtils.sFileUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on February 18, 2023
 */
public class APKDownloadingTask extends sExecutor {

    private final Activity mActivity;
    private final File mFile;
    private final LinearLayoutCompat mProgressLayout, mButtonsLayout;
    private final MaterialTextView mStatusText;
    private final ProgressBar mProgressBar;
    private final String mURL;

    public APKDownloadingTask(LinearLayoutCompat progressLayout, LinearLayoutCompat buttonsLayout, ProgressBar progressBar,
                              MaterialTextView statusText, String url, File file, Activity activity) {
        mProgressLayout = progressLayout;
        mButtonsLayout = buttonsLayout;
        mProgressBar = progressBar;
        mStatusText = statusText;
        mURL = url;
        mFile = file;
        mActivity = activity;
    }

    @Override
    public void onPreExecute() {
        Common.isDownloading(true);
        mStatusText.setText(mActivity.getString(R.string.downloading, Common.getAppName()));
        mButtonsLayout.setVisibility(View.GONE);
        mProgressLayout.setVisibility(View.VISIBLE);
        if (mFile.exists()) {
            sFileUtils.delete(mFile);
        }
    }

    @Override
    public void doInBackground() {
        try (FileOutputStream output = new FileOutputStream(mFile)) {
            URL apkURL = new URL(mURL);
            URLConnection connection = apkURL.openConnection();
            connection.connect();
            mProgressBar.setMax(connection.getContentLength());
            InputStream input = apkURL.openStream();
            byte[] data = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
                mProgressBar.setProgress((int) mFile.length());
            }
        } catch (IOException ignored) {}
    }

    @Override
    public void onPostExecute() {
        if (mFile.exists()) {
            new APKInstallingTask(mFile, mActivity).execute();
            mActivity.finish();
        } else {
            mProgressLayout.setVisibility(View.GONE);
            mButtonsLayout.setVisibility(View.VISIBLE);
            sCommonUtils.snackBar(mActivity.findViewById(android.R.id.content), mActivity.getString(R.string.download_failed)).show();
        }
        Common.isDownloading(false);
    }

}