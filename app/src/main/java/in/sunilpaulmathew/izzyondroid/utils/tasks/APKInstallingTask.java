package in.sunilpaulmathew.izzyondroid.utils.tasks;

import android.app.Activity;
import android.content.Intent;

import java.io.File;

import in.sunilpaulmathew.izzyondroid.activities.InstallerActivity;
import in.sunilpaulmathew.izzyondroid.services.InstallerService;
import in.sunilpaulmathew.izzyondroid.utils.ShizukuInstaller;
import in.sunilpaulmathew.sCommon.CommonUtils.sCommonUtils;
import in.sunilpaulmathew.sCommon.CommonUtils.sExecutor;
import in.sunilpaulmathew.sCommon.InstallerUtils.sInstallerParams;
import in.sunilpaulmathew.sCommon.InstallerUtils.sInstallerUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class APKInstallingTask extends sExecutor {

    private final Activity mActivity;
    private final File mAPKFile;

    public APKInstallingTask(File apkFile, Activity activity) {
        mAPKFile = apkFile;
        mActivity = activity;
    }

    @Override
    public void onPreExecute() {
        sCommonUtils.saveString("installationStatus", "waiting", mActivity);
        Intent installIntent = new Intent(mActivity, InstallerActivity.class);
        installIntent.putExtra(InstallerActivity.PATH_INTENT, mAPKFile.getAbsolutePath());
        mActivity.startActivity(installIntent);
    }

    @Override
    public void doInBackground() {
        if (ShizukuInstaller.isShizukuInstallation()) {
            ShizukuInstaller.installAPK(mAPKFile, mActivity);
        } else {
            int sessionId;
            final sInstallerParams installParams = sInstallerUtils.makeInstallParams(mAPKFile.length());
            sessionId = sInstallerUtils.runInstallCreate(installParams, mActivity);
            try {
                sInstallerUtils.runInstallWrite(mAPKFile.length(), sessionId, mAPKFile.getName(), mAPKFile.getAbsolutePath(), mActivity);
            } catch (NullPointerException ignored) {
            }
            sInstallerUtils.doCommitSession(sessionId, new Intent(mActivity, InstallerService.class), mActivity);
        }
    }

    @Override
    public void onPostExecute() {
        if (ShizukuInstaller.isShizukuInstallation()) {
            ShizukuInstaller.destroyProcess();
        }
    }

}