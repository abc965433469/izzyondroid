package in.sunilpaulmathew.izzyondroid.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.sCommon.CommonUtils.sCommonUtils;
import in.sunilpaulmathew.sCommon.PackageUtils.sPackageUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class RecyclerViewData {

    private static int getOrientation(Activity activity) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && activity.isInMultiWindowMode() ?
                Configuration.ORIENTATION_PORTRAIT : activity.getResources().getConfiguration().orientation;
    }

    public static int getSpanCount(int lSpan, int pSpan, Activity activity) {
        return getOrientation(activity) == Configuration.ORIENTATION_LANDSCAPE ? lSpan : pSpan;
    }

    public static List<RecyclerViewItems> getCategories(String category) {
        List<RecyclerViewItems> mData = new ArrayList<>();
        for (RecyclerViewItems item : Common.getRawData()) {
            if (PackageData.isCategoryApp(category, Objects.requireNonNull(PackageData.getCategories(item.getRawData())))) {
                if (Common.getSearchText() != null) {
                    if (Common.isTextMatched(item.getTitle())) {
                        mData.add(item);
                    }
                } else {
                    mData.add(item);
                }
            }
        }
        return mData;
    }

    public static List<RecyclerViewItems> getDeveloperApps() {
        List<RecyclerViewItems> mData = new ArrayList<>();
        try {
            for (RecyclerViewItems item : Common.getRawData()) {
                if (item.getAuthorName().equals(Common.getAuthorName())) {
                    mData.add(item);
                }
            }
        } catch (NullPointerException ignored) {}
        return mData;
    }

    public static List<RecyclerViewItems> getLatest(Context context) {
        List<RecyclerViewItems> mData = new ArrayList<>();
        long timeFrame = sCommonUtils.getLong("latestTimeFrame", 240, context);
        for (RecyclerViewItems item : Common.getRawData()) {
            if (System.currentTimeMillis() < timeFrame * 60 * 60 * 1000 + item.getLastUpdated()) {
                mData.add(item);
            }
        }
        return mData;
    }

    public static List<RecyclerViewItems> getInstalled(Context context) {
        List<RecyclerViewItems> mData = new ArrayList<>();
        for (RecyclerViewItems item : Common.getRawData()) {
            if (sPackageUtils.isPackageInstalled(item.getPackageName(), context)) {
                mData.add(item);
            }
        }
        return mData;
    }

    public static List<RecyclerViewItems> getSearchData() {
        List<RecyclerViewItems> mData = new ArrayList<>();
        for (RecyclerViewItems item : Common.getRawData()) {
            if (Common.getSearchText() != null) {
                if (Common.isTextMatched(item.getTitle())) {
                    mData.add(item);
                }
            } else {
                mData.add(item);
            }
        }
        return mData;
    }

    public static List<CategoryMenuItems> getCategories(Context context) {
        List<CategoryMenuItems> mData = new ArrayList<>();
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_system, context), sCommonUtils.getColor(R.color.colorAccent, context), "System"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_multimedia, context), sCommonUtils.getColor(R.color.colorRed, context), "Multimedia"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_connectivity, context), sCommonUtils.getColor(R.color.colorTeal, context), "Connectivity"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_science, context), sCommonUtils.getColor(R.color.colorBrown, context), "Science & Education"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_phone, context), sCommonUtils.getColor(R.color.colorBlueGrey, context), "Phone & SMS"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_development, context), sCommonUtils.getColor(R.color.colorPink, context), "Development"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_security, context), sCommonUtils.getColor(R.color.colorPurple, context), "Security"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_network, context), sCommonUtils.getColor(R.color.colorAccent, context), "Internet"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_reading, context), sCommonUtils.getColor(R.color.colorRed, context), "Reading"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_office, context), sCommonUtils.getColor(R.color.colorOrange, context), "Office"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_games, context), sCommonUtils.getColor(R.color.colorBrown, context), "Games"));
        mData.add(new CategoryMenuItems(sCommonUtils.getDrawable(R.drawable.ic_food, context), sCommonUtils.getColor(R.color.colorGreen, context), "Food"));
        return mData;
    }

}