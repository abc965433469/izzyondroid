* Improved handling json data so that main UI loading will now be much faster.
* Switched to Material3 DayNight theme (more beautiful UI).
* App now request permission to post notification on Android versions 13 and higher.
* App now shows a notification on acquiring update info.
* Improved handling screen rotation.
* Miscellaneous changes.